/**
 * TODO:
 * test garbage collection of template even if state is still in use by other templates
 */


import {validateDom} from "./DomValidator.js";
import htmel from "../src/htmel.js"
// import htmel from "../dist/htmel.min.js"

import assert from 'assert';
import {HtmelChecker} from "./HtmelChecker.js";

describe('text node', function () {
    it('static rendering', function (done) {
        let check = HtmelChecker(null, [
            {
                template: () => htmel()`${"123"}`,
                expected: () => `123`
            }, {
                template: () => htmel()`0${"123"}4`,
                expected: () => `01234`
            }, {
                template: () => htmel()`  0 ${"123"} 4      `,
                expected: () => `0 123 4`
            }, {
                template: () => htmel()`${"123"}4${567}`,
                expected: () => `1234567`
            }, {
                template: () => htmel()`<div>${"text"}</div>`,
                expected: () => `<div>text</div>`
            }
        ]);
        check()
        done();
    });

    it('basic updates', function (done) {
        let state = {
            a: "123"
        }

        let ELEMENT_CONST = "element";
        let elementStr = `<div>funshit</div>`;

        let calcA = () => state.a === ELEMENT_CONST ? htmel()`<div>funshit</div>` : state.a
        let expectA = () => state.a === ELEMENT_CONST ? elementStr : (state.a || "")

        let check = HtmelChecker(state, [
            {
                template: () => htmel(state)`${() => calcA()}`,
                expected: () => `${expectA()}`
            }, {
                template: () => htmel(state)`0${() => calcA()}4`,
                expected: () => `0${expectA()}4`
            }, {
                template: () => htmel(state)`  0 ${() => calcA()} 4      `,
                expected: () => `0 ${expectA()} 4`
            }, {
                template: () => htmel(state)`${() => calcA()}4${() => calcA()}`,
                expected: () => `${expectA()}4${expectA()}`
            }, {
                template: () => htmel(state)`<div>${() => calcA()}</div>`,
                expected: () => `<div>${expectA()}</div>`
            }
        ]);
        check();
        state.a = 666;
        check();
        state.a = null;
        check();
        state.a = false;
        check();
        state.a = true;
        check();
        state.a = ELEMENT_CONST;
        check();
        state.a = "test_123";
        check();

        done();
    });

    it('type transformations', function (done) {
        let state = {
            value: null
        }
        let LIST_MARKER = "<htmel-list-location-marker></htmel-list-location-marker>";
        let expected = "";

        let transformations = {
            STRING: () => {
                state.value = "string here";
                expected = "string here";
            },
            NULL: () => {
                state.value = null;
                expected = "";
            },
            ELEMENT: () => {
                let e = document.createElement("div");
                e.textContent = "asd"
                state.value = e;
                expected = "<div>asd</div>";
            },
            TEXT_NODE: () => {
                state.value = document.createTextNode("shitfuck");
                expected = "shitfuck";
            },
            DOCUMENT_FRAGMENT_NO_CHILDREN: () => {
                state.value = htmel()``;
                expected = "";
            },
            DOCUMENT_FRAGMENT_ONE_CHILD: () => {
                state.value = htmel()`<div>asd</div>`;
                expected = "<div>asd</div>";
            },
            DOCUMENT_FRAGMENT_MANY_CHILDREN: () => {
                state.value = htmel()`<div>asd</div> asd2 ${null}${"asd_node"}<div>asd3</div>`;
                expected = `<div>asd</div> asd2 asd_node<div>asd3</div>${LIST_MARKER}`;
            },
            DOCUMENT_FRAGMENT_TEXT_NODE_CHILD: () => {
                state.value = htmel()`asd`;
                expected = "asd";
            },
            EMPTY_ARRAY: () => {
                state.value = [];
                expected = LIST_MARKER;
            },
            STRING_ARRAY: () => {
                state.value = ["asd1", "asd2"];
                expected = `asd1asd2${LIST_MARKER}`;
            },
            NODE_ARRAY: () => {
                let e = document.createElement("div");
                e.textContent = "lefet"
                state.value = [e, document.createTextNode("beef")];
                expected = `<div>lefet</div>beef${LIST_MARKER}`;
            },
            DOCUMENT_FRAGMENT_ARRAY: () => {
                state.value = [htmel()`<div>asd</div>`, htmel()`asd2`, htmel()`<div>asd3</div>asd4`];
                expected = `<div>asd</div>asd2<div>asd3</div>asd4${LIST_MARKER}`;
            },
            MIXED_ARRAY: () => {
                let e = document.createElement("div");
                e.textContent = "element";

                state.value = [
                    htmel()`<div>asd</div>`,
                    document.createTextNode("textnode"),
                    htmel()`asd2`,
                    e,
                    "strstr",
                    null,
                    htmel()`<div>asd3</div>asd4`
                ];
                expected =
                    `<div>asd</div>`
                    + `textnode`
                    + `asd2`
                    + `<div>element</div>`
                    + `strstr`
                    + `<div>asd3</div>asd4${LIST_MARKER}`
            },
        }

        let check = HtmelChecker(state, [
            {
                template: () => htmel(state)`${() => state.value}`,
                expected: () => expected
            }
        ]);
        check();

        for (let transformationName of Object.keys(transformations)) {
            let transformation = transformations[transformationName];
            transformation();
            try {
                check()
            } catch (e) {
                throw new Error(`Failed transformation: ${transformationName}\n${e}`)
            }
        }

        done();
    });

    describe('rerendering', function () {
        it("shouldn't rerender", function (done) {
            let outerRenders = 0;
            let innerRenders = 0;

            let state = {
                a: "some value"
            }

            let templateResult = null

            let check = HtmelChecker(state, [
                {
                    template: () => htmel(state)`
                    <div>
                    ${() => {
                        outerRenders += 1;
                        templateResult = htmel(state)`
                        ${() => {
                            innerRenders += 1;
                            return state.a
                        }}
                        `;
                        return templateResult
                    }}
                    </div>
                    `,
                    expected: () => `<div>${state.a}</div>`
                }
            ]);
            check();
            assert.equal(outerRenders, 1);
            assert.equal(innerRenders, 1);

            outerRenders = 0;
            innerRenders = 0;

            let oldTemplateResult = templateResult;
            state.a = "new value";
            check();
            assert.equal(oldTemplateResult, templateResult);
            assert.equal(outerRenders, 0);
            assert.equal(innerRenders, 1);

            done();
        });

        it("shouldn't rerender nested", function (done) {
            let outerOuterRenders = 0;
            let outerRenders = 0;
            let innerRenders = 0;

            let state = {
                a: "some value"
            }

            let outerTemplateResult = null
            let innerTemplateResult = null

            let check = HtmelChecker(state, [
                {
                    template: () => htmel(state)`
                    <div>
                    ${() => {
                        outerOuterRenders += 1;
                        outerTemplateResult = htmel()`
                        ${() => {
                            outerRenders += 1;
                            innerTemplateResult = htmel(state)`
                            ${() => {
                                innerRenders += 1;
                                return state.a
                            }}
                            `;
                            return innerTemplateResult
                        }}
                        `;
                        return outerTemplateResult
                    }}
                    </div>
                    `,
                    expected: () => `<div>${state.a}</div>`
                }
            ]);
            check();
            assert.equal(outerOuterRenders, 1);
            assert.equal(outerRenders, 1);
            assert.equal(innerRenders, 1);

            outerOuterRenders = 0;
            outerRenders = 0;
            innerRenders = 0;

            let oldOuterTemplateResult = outerTemplateResult;
            let oldInnerTemplateResult = innerTemplateResult;
            state.a = "new value";
            check();
            assert.equal(oldOuterTemplateResult, outerTemplateResult);
            assert.equal(oldInnerTemplateResult, innerTemplateResult);
            assert.equal(outerOuterRenders, 0);
            assert.equal(outerRenders, 0);
            assert.equal(innerRenders, 1);

            done();
        });

        it("should rerender without cache", function (done) {
            let outerRenders = 0;
            let innerRenders = 0;

            let state = {
                a: "some value"
            }

            let templateResult = null

            let check = HtmelChecker(state, [
                {
                    template: () => htmel(state)`
                    <div>
                    ${() => {
                        outerRenders += 1;
                        templateResult = htmel()`
                        ${() => {
                            innerRenders += 1;
                            return state.a
                        }}
                        `;
                        return templateResult
                    }}
                    </div>
                    `,
                    expected: () => `<div>${state.a}</div>`
                }
            ]);
            check();
            assert.equal(outerRenders, 1);
            assert.equal(innerRenders, 1);

            outerRenders = 0;
            innerRenders = 0;

            let oldTemplateResult = templateResult;
            state.a = "new value";
            check();
            assert.notEqual(oldTemplateResult, templateResult);
            assert.equal(outerRenders, 1);
            assert.equal(innerRenders, 1);

            done();
        });

        it("should rerender with cache", function (done) {
            let outerRenders = 0;
            let innerRenders = 0;

            let state = {
                a: "some value"
            }

            let templateResult = null

            let check = HtmelChecker(state, [
                {
                    template: () => htmel(state)`
                    <div>
                    ${() => {
                        outerRenders += 1;
                        // Just to access state.a to trigger rerender
                        let b = state.a;
                        templateResult = htmel(state)`
                        ${() => {
                            innerRenders += 1;
                            return state.a
                        }}
                        `;
                        return templateResult
                    }}
                    </div>
                    `,
                    expected: () => `<div>${state.a}</div>`
                }
            ]);
            check();
            assert.equal(outerRenders, 1);
            assert.equal(innerRenders, 1);

            outerRenders = 0;
            innerRenders = 0;

            let oldTemplateResult = templateResult;
            state.a = "new value";
            check();
            assert.equal(oldTemplateResult, templateResult);
            assert.equal(outerRenders, 1);
            assert.equal(innerRenders, 1);

            done();
        });

        it("different states", function (done) {
            let outerRenders = 0;
            let innerRenders = 0;

            let outerState = {
                a: "outer value",
            }
            let innerState = {
                a: "inner value",
            }

            let templateResult = null

            let check = HtmelChecker(outerState, [
                {
                    template: () => htmel(outerState)`
                    <div>
                    ${() => {
                        outerRenders += 1;
                        templateResult = htmel(innerState)`
                        ${() => {
                            innerRenders += 1;
                            return outerState.a + innerState.a
                        }}
                        `;
                        return templateResult
                    }}
                    </div>
                    `,
                    expected: () => `<div>${outerState.a + innerState.a}</div>`
                }
            ]);
            check();
            assert.equal(outerRenders, 1);
            assert.equal(innerRenders, 1);

            outerRenders = 0;
            innerRenders = 0;

            let oldTemplateResult = templateResult;
            outerState.a = "new outer value";
            check();
            assert.notEqual(oldTemplateResult, templateResult);
            assert.equal(outerRenders, 1);
            assert.equal(innerRenders, 1);

            outerRenders = 0;
            innerRenders = 0;

            oldTemplateResult = templateResult;
            innerState.a = "new inner value";
            check();
            assert.equal(oldTemplateResult, templateResult);
            assert.equal(outerRenders, 0);
            assert.equal(innerRenders, 1);

            done();
        });
    });
});

describe('attribute node', function () {
    it('static rendering', function (done) {
        let check = HtmelChecker(null, [
            { // Static attribute value
                template: () => htmel()`<div dir=${"rtl"}></div>`,
                expected: () => `<div dir="rtl"></div>`
            }, { // Static attribute name
                template: () => htmel()`<div ${"attr"}></div>`,
                expected: () => `<div attr></div>`
            }, { // Static dict attribute
                template: () => htmel()`<div ${{a: 3, b: "asd"}}></div>`,
                expected: () => `<div a="3" b="asd"></div>`
            }, { // Static empty dict attribute
                template: () => htmel()`<div ${{}}></div>`,
                expected: () => `<div></div>`
            }, { // Static null attr name
                template: () => htmel()`<div ${null}></div>`,
                expected: () => `<div></div>`
            }
        ]);
        check();
        done()
    });
    it('basic updates', function (done) {
        let state = {
            a: "rtl"
        }
        let check = HtmelChecker(state, [
            { // Attribute
                template: () => htmel(state)`<div dir=${() => state.a}></div>`,
                expected: () => state.a ? `<div dir="${state.a === true ? "" : state.a}"></div>` : `<div></div>`
            }, { // Attribute with quotes
                template: () => htmel(state)`<div dir="${() => state.a}"></div>`,
                expected: () => state.a ? `<div dir="${state.a === true ? "" : state.a}"></div>` : `<div></div>`
            }, { // Part of attribute
                template: () => htmel(state)`<div dir=a${() => state.a}a></div>`,
                expected: () => `<div dir="a${state.a}a"></div>`
            }, { // Part of attribute with quotes
                template: () => htmel(state)`<div dir="a ${() => state.a} a"></div>`,
                expected: () => `<div dir="a ${state.a} a"></div>`
            }, { // Twice in the same attribute
                template: () => htmel(state)`<div dir="${() => state.a} a ${() => state.a}"></div>`,
                expected: () => `<div dir="${state.a} a ${state.a}"></div>`
            }, { // Two attributes with the same prop
                template: () => htmel(state)`<div a1=${() => state.a} a2=${() => state.a}></div>`,
                expected: () => state.a ? `<div a1="${state.a === true ? "" : state.a}" a2="${state.a === true ? "" : state.a}"></div>` : `<div></div>`
            }, { // Attr name
                template: () => htmel(state)`<div ${() => state.a}></div>`,
                expected: () => state.a ? `<div ${state.a}></div>` : `<div></div>`
            }, { // Part of attr name
                template: () => htmel(state)`<div a${() => state.a}a></div>`,
                expected: () => `<div a${state.a}a></div>`
            }, { // Attr dict
                template: () => htmel(state)`<div ${() => ({a: state.a, b: state.a})}></div>`,
                expected: () => state.a ? `<div a="${state.a === true ? "" : state.a}" b="${state.a === true ? "" : state.a}"></div>` : `<div></div>`
            }, { // Attr dict with static value
                template: () => htmel(state)`<div ${() => ({a: state.a, b: "smth"})}></div>`,
                expected: () => state.a ? `<div a=${state.a === true ? '""' : state.a} b="smth"></div>` : `<div b="smth"></div>`
            },
        ]);

        check();
        state.a = "ltr"
        check();
        state.a = null;
        check();
        state.a = false;
        check();
        state.a = true;
        check();
        state.a = "ltr"
        check();

        done()
    });

    describe('event handlers', function () {
        it('event firing', function (done) {
            let clicked = false;

            let check = HtmelChecker(null, [
                { // Static event handler
                    template: () => htmel()`<div onclick="${() => clicked = true}"></div>`,
                    expected: () => `<div></div>`,
                    validator: (_, e) => {
                        e.click();
                        assert.equal(clicked, true);
                        clicked = false;
                    }
                }, { // Static event handler that returns function
                    template: () => htmel()`<div onclick="${() => () => clicked = true}"></div>`,
                    expected: () => `<div></div>`,
                    validator: (_, e) => {
                        e.click();
                        assert.equal(clicked, true);
                        clicked = false;
                    }
                }
            ]);

            check();

            done()
        });
        it('changing handler', function (done) {
            let clicked = false;
            let superClick = false;
            let state = {
                func: () => clicked = true
            }
            let check = HtmelChecker(state, [
                { // Static event handler
                    template: () => htmel(state)`<div onclick="${() => state.func()}"></div>`,
                    expected: () => `<div></div>`,
                    validator: (_, e) => {
                        e.click();
                        assert.equal(clicked, true);
                        clicked = false;
                    }
                }
            ]);

            check();

            state.func = () => {
                clicked = true;
                superClick = true;
            }
            check()
            assert.equal(superClick, true);

            done()
        });
    });

    it('convert attributes to properties', function (done) {
        let state = {
            a: "asd",
            obj: {
                a: "asdf"
            },
            attrsDict: {
                prop: {
                    a: "nothing"
                }
            }
        }
        let isObj = o => ["function", "object"].includes(typeof o);

        let check = HtmelChecker(state, [
            { // Static obj attr
                template: () => htmel()`<div prop=${{a: "static"}}></div>`,
                expected: () => `<div prop="__obj_placeholder__"></div>`,
                validator: (_, e) => assert.equal(e.prop.a, "static")
            }, { // Obj attr
                template: () => htmel(state)`<div prop=${() => ({a: state.a})}></div>`,
                expected: () => `<div prop="__obj_placeholder__"></div>`,
                validator: (_, e) => assert.equal(e.prop.a, state.a)
            }, { // Obj attr
                template: () => htmel(state)`<div prop=${() => state.obj}></div>`,
                expected: () => state.obj == null ? `<div></div>` : (
                    isObj(state.obj) ? `<div prop="__obj_placeholder__"></div>` : `<div prop="${state.obj}"></div>`
                ),
                validator: (_, e) => state.obj == null ? assert.equal(e.prop, null) : (
                    isObj(state.obj) ? assert.equal(e.prop, state.obj) : assert.equal(e.prop, null)
                )
            }, {
                template: () => htmel(state)`<div ${() => state.attrsDict}></div>`,
                expected: () => (state.attrsDict == null || state.attrsDict.prop == null) ? `<div></div>` : (
                    isObj(state.attrsDict.prop) ? `<div prop="__obj_placeholder__"></div>`
                        : `<div prop="${state.attrsDict.prop}"></div>`
                ),
                validator: (_, e) => (state.attrsDict == null || state.attrsDict.prop == null) ?
                    assert.equal(e.prop, null)
                    :
                    (
                        isObj(state.attrsDict.prop) ? assert.equal(e.prop, state.attrsDict.prop) : assert.equal(e.prop, null)
                    )
            },
        ]);
        check();
        state.a = "asdf";
        state.obj = {
            a: "asdf2"
        }
        state.attrsDict = {
            prop: {
                a: "nothing 2"
            }
        }
        check();

        state.a = null;
        state.obj = null
        state.attrsDict = null
        check();

        state.obj = "sum string"
        state.attrsDict = {
            prop: "stuff"
        }
        check()

        state.obj = () => doStuff()
        state.attrsDict = {
            prop: () => doStuff()
        }
        check();

        state.attrsDict = {
            prop: null
        }
        check()

        done();
    })

    it('illegal attributes', function (done) {
        assert.throws(() => htmel()`<div ${"123"}></div>`,
            Error, "Numeric attr name should throw an error");
        assert.throws(() => htmel()`<div ${"123asd"}></div>`,
            Error, "Numeric attr name should throw an error");

        done()
    });
});
